# Sample responses from UH-SCIM implementations

## UH-IAM at UiB (RapidIdentity, G3)

### `GET .../Users?userName=gaa041`

This is an example of the non-standard query to look up an account by user name.
This returns the same kind of response you would get from the corresponding `filter`-query.
You should expect a `ListResponse` with a single user present.

At the HTTP level this is a `200 OK` response with `content-type: application/scim+json`. The body looks like this after re-formatting:

```json
{
  "schemas": [
    "urn:ietf:params:scim:api:messages:2.0:ListResponse"
  ],
  "totalResults": 1,
  "startIndex": 1,
  "itemsPerPage": 1,
  "Resources": [
    {
      "schemas": [
        "urn:ietf:params:scim:schemas:core:2.0:User",
        "urn:ietf:params:scim:schemas:extension:enterprise:2.0:User",
        "no:edu:scim:user"
      ],
      "id": "28efb461-a89e-49b1-a670-77fd89d8031a",
      "externalId": "28efb461-a89e-49b1-a670-77fd89d8031a",
      "userName": "gaa041@uib.no",
      "name": {
        "givenName": "Gisle",
        "familyName": "Aas",
        "formatted": "Gisle Aas"
      },
      "displayName": "Gisle Aas",
      "profileUrl": "https://www.uib.no/user/uib/gaa041",
      "title": "Sjefingeniør",
      "userType": "Employee",
      "preferredLanguage": null,
      "active": true,
      "emails": [
        {
          "value": "gisle.aas@uib.no",
          "type": "work"
        }
      ],
      "phoneNumbers": [
        {
          "value": "+47 93241450",
          "type": "mobile"
        }
      ],
      "roles": [
        {
          "value": "iam:manager"
        },
        {
          "value": "iam:operation"
        },
        {
          "value": "iam:student"
        },
        {
          "value": "iam:employee"
        }
      ],
      "addresses": [
        {
          "formatted": "Nygårdsgaten 5\n5015 Bergen",
          "streetAddress": "Nygårdsgaten 5",
          "locality": "Bergen",
          "postalCode": "5015",
          "country": null,
          "type": "work"
        }
      ],
      "urn:ietf:params:scim:schemas:extension:enterprise:2.0:User": {
        "employeeNumber": "06056209",
        "costCenter": null,
        "organization": null,
        "division": null,
        "department": null,
        "manager": {
          "value": "f2b55bff-f398-404e-b692-4913a2f6a3ac",
          "$ref": "https://gw-uib.intark.uh-it.no/iga/scim/v2/Users/f2b55bff-f398-404e-b692-4913a2f6a3ac",
          "displayName": "Jon Eikhaug"
        }
      },
      "no:edu:scim:user": {
        "accountType": "primary",
        "employeeNumber": "06056209",
        "studentNumber": "329694",
        "fsPersonNumber": "1255615",
        "gregPersonNumber": null,
        "eduPersonPrincipalName": "gaa041@uib.no",
        "userPrincipalName": "gisle.aas@uib.no",
        "primaryOrgUnit": {
          "symbol": "ADM-IT-ARK",
          "legacyStedkode": "229911",
          "nameNb": "Arkitektur og skysatsing",
          "nameEn": null
        }
      },
      "meta": {
        "resourceType": "User",
        "created": "2023-10-13T12:59:14Z",
        "lastModified": "2023-12-06T10:41:53Z",
        "location": "https://gw-uib.intark.uh-it.no/iga/scim/v2/Users/28efb461-a89e-49b1-a670-77fd89d8031a"
      }
    }
  ]
}
```

## UH-IAM at UiB (RapidIdentity, G1)

### `GET .../Users?userName=gaa041`

This is an example of the non-standard query to look up an account by user name.
This returns the same kind of response you would get from the corresponding `filter`-query.
You should expect a `ListResponse` with a single user present.

At the HTTP level this is a `200 OK` response with `content-type: application/scim+json`. The body looks like this after re-formatting:

```json
{
  "schemas": [
    "urn:ietf:params:scim:api:messages:2.0:ListResponse"
  ],
  "totalResults": 1,
  "startIndex": 1,
  "itemsPerPage": 1,
  "Resources": [
    {
      "schemas": [
        "urn:ietf:params:scim:schemas:core:2.0:User",
        "urn:ietf:params:scim:schemas:extension:enterprise:2.0:User",
        "no:edu:scim:user"
      ],
      "id": "28efb461-a89e-49b1-a670-77fd89d8031a",
      "externalId": "28efb461-a89e-49b1-a670-77fd89d8031a",
      "userName": "gaa041@uib.no",
      "name": {
        "givenName": "Gisle",
        "familyName": "Aas",
        "formatted": "Gisle Aas"
      },
      "displayName": "Gisle Aas",
      "profileUrl": "https://www.uib.no/user/uib/gaa041",
      "title": "Sjefingeniør",
      "userType": "Employee",
      "preferredLanguage": null,
      "active": true,
      "emails": [
        {
          "value": "gisle.aas@uib.no",
          "type": "work"
        }
      ],
      "phoneNumbers": [
        {
          "value": "+47 93241450",
          "type": "mobile"
        }
      ],
      "roles": [
        {
          "value": "iam:manager"
        },
        {
          "value": "iam:operation"
        },
        {
          "value": "iam:employee"
        }
      ],
      "addresses": [
        {
          "formatted": "Kalfarveien 31\n5018 Bergen",
          "streetAddress": "Kalfarveien 31",
          "locality": "Bergen",
          "postalCode": "5018",
          "country": null,
          "type": "work"
        }
      ],
      "urn:ietf:params:scim:schemas:extension:enterprise:2.0:User": {
        "employeeNumber": "06056209",
        "costCenter": null,
        "organization": null,
        "division": null,
        "department": null,
        "manager": {
          "value": "f2b55bff-f398-404e-b692-4913a2f6a3ac"
        }
      },
      "no:edu:scim:user": {
        "accountType": "primary",
        "employeeNumber": "06056209",
        "studentNumber": null,
        "fsPersonNumber": null,
        "eduPersonPrincipalName": "gaa041@uib.no",
        "userPrincipalName": "gisle.aas@uib.no",
        "primaryOrgUnit": {
          "symbol": "ADM-IT-ARK",
          "legacyStedkode": "229911",
          "nameNb": "Arkitektur og skysatsing",
          "nameEn": null
        },
        "gregPersonNumber": null
      },
      "meta": {
        "resourceType": "User",
        "created": "2022-03-24T10:22:18Z",
        "lastModified": "2023-06-02T02:40:04Z",
        "location": "https://gw-uib.intark.uh-it.no/iga/scim/v2/Users/28efb461-a89e-49b1-a670-77fd89d8031a"
      }
    }
  ]
}
```

### `/Users?attributes=userName%2Cname.familyName&userName=gaa041`

In this example we limit the attributes to be returned by passing in the `attributes`-query parameter.
The `id` attribute is always present even if you don't explicitly ask for it.

```json
{
  "schemas": [
    "urn:ietf:params:scim:api:messages:2.0:ListResponse"
  ],
  "totalResults": 1,
  "startIndex": 1,
  "itemsPerPage": 1,
  "Resources": [
    {
      "id": "28efb461-a89e-49b1-a670-77fd89d8031a",
      "userName": "gaa041@uib.no",
      "name": {
        "familyName": "Aas"
      }
    }
  ]
}
```

### `GET .../Users?userName=xxx9999`

This shows the response you get when the user doesn't exit.  It's still a `200 OK` response with the following content.

```json
{
  "schemas": [
    "urn:ietf:params:scim:api:messages:2.0:ListResponse"
  ],
  "totalResults": 0,
  "startIndex": 1,
  "itemsPerPage": 0,
  "Resources": []
}
```

### `GET .../Users/28efb461-a89e-49b1-a670-77fd89d8031a`

This is the standard lookup of a specific account by `id`. It returns the user object directly. The output has been truncated, see one of the examples above for the full user object of `gaa041`.

```json
{
  "schemas": [
    "urn:ietf:params:scim:schemas:core:2.0:User",
    "urn:ietf:params:scim:schemas:extension:enterprise:2.0:User",
    "no:edu:scim:user"
  ],
  "id": "28efb461-a89e-49b1-a670-77fd89d8031a",
  "externalId": "28efb461-a89e-49b1-a670-77fd89d8031a",
  "userName": "gaa041@uib.no",
  ...
}
```

### Example event messages

This is an example of message emitted to RabbitMQ when attributes of a user account are updated. It's
updated with the topic `no.uib.iga.scim.user.modify`.

```json
{
  "attributes": [
    "meta.lastModified",
    "phoneNumbers[type eq \"mobile\"]"
  ],
  "resourceUris": [
    "https://gw-uib.intark.uh-it.no/iga/scim/v2/Users/5039f59e-c5dc-4c84-882c-b5cba78301b2"
  ],
  "schemas": [
    "urn:ietf:params:scim:schemas:notify:2.0:Event"
  ],
  "time": "2023-06-30T06:22:01Z",
  "type": "MODIFY"
}
```

This example how the message emitted when a new account is created (topic `no.uib.iga.scim.user.add`):

```json
{
  "attributes": [
    "meta.created",
    "name.formatted",
    "name.givenName",
    "id",
    "no:edu:scim:user:accountType",
    "userName",
    "no:edu:scim:user:norEduPersonNIN",
    "userType",
    "no:edu:scim:user:fsPersonNumber",
    "no:edu:scim:user:studentNumber",
    "active",
    "meta.lastModified",
    "name.familyName"
  ],
  "resourceUris": [
    "https://gw-uib.intark.uh-it.no/iga/scim/v2/Users/ac4c986f-22c7-496b-9f3d-85292c20e4e9"
  ],
  "schemas": [
    "urn:ietf:params:scim:schemas:notify:2.0:Event"
  ],
  "time": "2023-06-30T06:24:01Z",
  "type": "ADD"
}
```

When accounts activate/deactivate we used to get a message like this (topic `no.uib.iga.scim.user.activate`) in RI-G1, but RI-G3 never implemented these:

```json
{
  "resourceUris": [
    "https://gw-uib.intark.uh-it.no/iga/scim/v2/Users/8e267acc-b92d-440f-aa27-8b79404da770"
  ],
  "schemas": [
    "urn:ietf:params:scim:schemas:notify:2.0:Event"
  ],
  "time": "2023-06-29T22:19:02Z",
  "type": "ACTIVATE"
}
```

## NTNU UH-Sak SCIM adaptor

### `GET .../Users?norEduPersonNIN=101274XXXXX`

Example of lookup up a person using its _fødselsnummer_ (rendered in censored form both in the request line and the response body).
The response code is `200 OK` with a header of `content-type: application/scim+json` and body like this:

```json
{
  "Resources": [
    {
      "active": true,
      "displayName": "Kent Overholdt",
      "emails": [
        {
          "value": "kent.overholdt@ntnu.no",
          "type": "work",
          "primary": true
        }
      ],
      "id": "overhold@ntnu.no",
      "locale": "nb-NO",
      "meta": {
        "resourceType": "Users",
        "location": "Users/overhold@ntnu.no"
      },
      "name": {
        "formatted": "Kent Overholdt",
        "familyName": "Overholdt",
        "givenName": "Kent"
      },
      "no:edu:scim:user": {
        "eduPersonPrincipalName": "overhold@ntnu.no",
        "employeeNumber": "06143937",
        "fsPersonNumber": "127009",
        "gregPersonNumber": "19719",
        "norEduPersonNIN": "101274XXXXX",
        "primaryOrgUnit": {
          "orgregOuId": 1206,
          "symbol": "OI-IT-DRIFT"
        },
        "roles": [
          {
            "value": "staff",
            "type": "eduPersonAffiliation"
          },
          {
            "value": "affiliate",
            "type": "eduPersonAffiliation"
          },
          {
            "value": "employee",
            "type": "eduPersonAffiliation"
          },
          {
            "value": "staff",
            "type": "eduPersonPrimaryAffiliation"
          }
        ],
        "studentNumber": "638360",
        "userPrincipalName": "kent.overholdt@ntnu.no"
      },
      "phoneNumbers": [
        {
          "value": "+4741304361",
          "type": "mobile"
        }
      ],
      "preferredLanguage": "nb-NO",
      "schemas": [
        "urn:ietf:params:scim:schemas:core:2.0:User",
        "no:edu:scim:user"
      ],
      "timezone": "Europe/Oslo",
      "title": "Overingeniør",
      "userName": "overhold@ntnu.no"
    }
  ],
  "itemsPerPage": 1000,
  "schemas": [
    "urn:ietf:params:scim:api:messages:2.0:ListResponse"
  ],
  "startIndex": 1,
  "totalResults": 1
}
```

### `GET .../Users?startIndex=2&count=1`

Example from the UH-Sak spec Word document that extract a test account using paging:

```json
{
  "Resources": [
    {
      "active": true,
      "displayName": "Lars Test",
      "emails": [
        {
          "value": "lars.test@ntnu.no",
          "type": "work",
          "primary": true
        }
      ],
      "id": "lars@ntnu.no",
      "locale": "nb-NO",
      "meta": {
        "resourceType": "User",
        "location": "Users/lars@ntnu.no"
      },
      "name": {
        "formatted": "Lars Test",
        "familyName": "Test",
        "givenName": "Lars"
      },
      "no:edu:scim:user": {
        "eduPersonPrincipalName": "lars@ntnu.no",
        "employeeNumber": "99999999",
        "fsPersonNumber": "99999999",
        "norEduPersonNIN": "99999999999",
        "primaryOrgUnit": {
          "orgregOuId": 999999,
          "symbol": "OD-IT-DRIFT"
        },
        "roles": [
          {
            "value": "employee",
            "type": "eduPersonAffiliation"
          },
          {
            "value": "staff",
            "type": "eduPersonAffiliation"
          },
          {
            "value": "staff",
            "type": "eduPersonPrimaryAffiliation"
          }
        ],
        "userPrincipalName": "lars.test@ntnu.no"
      },
      "phoneNumbers": [
        {
          "value": "+47 99999999",
          "type": "mobile"
        }
      ],
      "preferredLanguage": "nb-NO",
      "schemas": [
        "urn:ietf:params:scim:schemas:core:2.0:User",
        "no:edu:scim:user"
      ],
      "timezone": "Europe/Oslo",
      "title": "Senioringeniør",
      "userName": "lars@ntnu.no"
    }
  ],
  "itemsPerPage": 1,
  "schemas": [
    "urn:ietf:params:scim:api:messages:2.0:ListResponse"
  ],
  "startIndex": 2,
  "totalResults": 1
}
```