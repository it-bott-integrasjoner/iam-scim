# UH-SCIM

This repository contains specifications of the SCIM API to provided by IGA
implementations in the Norwegian higher education sector. Our main use case for
this API is to provide for IntArk-style provisioning of user accounts by
exposing the state of the accounts managed.

SCIM is also frequently used as a provision target — to push account information
onto a system that accepts updates via SCIM. That use case is *not* covered by the
specified subset.

SCIM is a standard HTTP/JSON based API defined by [RFC
7643](https://tools.ietf.org/html/rfc7643).  This specification describes a
subset of the standard as a minimal baseline to be implemented, some extension
query parameters, as well as an extension schema (`no:edu:scim:user`) for additional user attributes.
We also specify a message format to use for events indicating changes to the data set.

## Specifications

* [UH-SCIM v1.0](UH-SCIM-v1.0.md) – the original spec developed in 2020 as basis for the UH IAM project pilot at UiB.
* [UH-SCIM v2.x](UH-SCIM-v2.0-tobe.md) – work in progress – an update based on experience with 1.0, requirements from UH-Sak and what's doable in the cloud version of UH IAM (RI).

## Implementations

* SEBRA SCIM (decommissioned)
* UH IAM (RI-sky, G3) at UiB available from [UiBs API GW](https://api-uib.intark.uh-it.no/catalog/api/91a73d99-d9b2-452a-a73d-99d9b2e52a9a) implements UH-SCIM v1.0 with some additions:
    - exposed `profileUrl`, `preferredLanguage`, `addresses` attributes
    - added `primaryOrgUnit` to `no:edu:scim:user`.
* NTNU UH-Sak pilot implements UH-SCIM v1.0 with some other additions
* DFØ SCIM

We also provide some [sample responses](sample-responses.md) from some of these implementations.